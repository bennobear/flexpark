import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import java.sql.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PL�schen extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PL�schen frame = new PL�schen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PL�schen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		this.setTitle("L�schung - P");

		JLabel lblFreieParkpltze = new JLabel("Freie Parkpl\u00E4tze");
		GridBagConstraints gbc_lblFreieParkpltze = new GridBagConstraints();
		gbc_lblFreieParkpltze.insets = new Insets(0, 0, 5, 5);
		gbc_lblFreieParkpltze.gridx = 0;
		gbc_lblFreieParkpltze.gridy = 1;
		contentPane.add(lblFreieParkpltze, gbc_lblFreieParkpltze);

		JLabel lblWelchenParkplatzWollen = new JLabel("Welchen Parkplatz wollen sie l\u00F6schen?");
		GridBagConstraints gbc_lblWelchenParkplatzWollen = new GridBagConstraints();
		gbc_lblWelchenParkplatzWollen.insets = new Insets(0, 0, 5, 0);
		gbc_lblWelchenParkplatzWollen.gridx = 5;
		gbc_lblWelchenParkplatzWollen.gridy = 1;
		contentPane.add(lblWelchenParkplatzWollen, gbc_lblWelchenParkplatzWollen);

		JLabel lblanzahl = new JLabel("[Anzahl]");
		GridBagConstraints gbc_lblanzahl = new GridBagConstraints();
		gbc_lblanzahl.insets = new Insets(0, 0, 5, 5);
		gbc_lblanzahl.gridx = 0;
		gbc_lblanzahl.gridy = 2;
		contentPane.add(lblanzahl, gbc_lblanzahl);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			String query = "SELECT COUNT(istBesetzt) FROM Parkplatz WHERE istBesetzt < '1'";
			PreparedStatement PS = CC.prepareStatement(query);
			ResultSet result = PS.executeQuery();
			result.next();
			lblanzahl.setText("" + result.getString(1));

			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JButton btnZurck = new JButton("Zur\u00FCck");
		btnZurck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PVerwalten p = new PVerwalten();
				p.setVisible(true);
				dispose();

			}
		});
		GridBagConstraints gbc_btnZurck = new GridBagConstraints();
		gbc_btnZurck.insets = new Insets(0, 0, 5, 5);
		gbc_btnZurck.gridx = 0;
		gbc_btnZurck.gridy = 3;
		contentPane.add(btnZurck, gbc_btnZurck);

		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 5;
		gbc_comboBox.gridy = 3;
		contentPane.add(comboBox, gbc_comboBox);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			Statement stmt = CC.createStatement();
			String liste = "SELECT * FROM Parkplatz";
			ResultSet rs = stmt.executeQuery(liste);
			while (rs.next()) {
				comboBox.addItem(rs.getString("Bezeichnung"));

			}
			stmt.close();

			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JButton btnFortfahren = new JButton("Fortfahren");
		btnFortfahren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int y = comboBox.getSelectedIndex() + 1;
				System.out.println(y);

				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection CC = null;
					CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
					System.out.print("Database is connected!");

					String update = ("DELETE FROM `Parkplatz`  WHERE '" + y + "' = idParkplatz");
					PreparedStatement PS = CC.prepareStatement(update);
					int result = PS.executeUpdate();

					CC.close();
				} catch (Exception e1) {
					System.out.print("Connection to Database failed - Error:" + e1);
				}

				Men� m1 = new Men�();
				m1.setVisible(true);
				Popup5 p = new Popup5();
				p.setVisible(true);
				dispose();

			}
		});
		GridBagConstraints gbc_btnFortfahren = new GridBagConstraints();
		gbc_btnFortfahren.gridx = 5;
		gbc_btnFortfahren.gridy = 6;
		contentPane.add(btnFortfahren, gbc_btnFortfahren);
		this.setTitle("Parkplatz L�schung");
	}
}
