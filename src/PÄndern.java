import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class P�ndern extends JFrame {

	private JPanel contentPane;
	private JTextField editBeschreibung;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					P�ndern frame = new P�ndern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public P�ndern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		this.setTitle("Daten Bearbeitung - P");

		JLabel lblFreieParkpltze = new JLabel("Freie Parkpl\u00E4tze");
		GridBagConstraints gbc_lblFreieParkpltze = new GridBagConstraints();
		gbc_lblFreieParkpltze.insets = new Insets(0, 0, 5, 5);
		gbc_lblFreieParkpltze.gridx = 0;
		gbc_lblFreieParkpltze.gridy = 1;
		contentPane.add(lblFreieParkpltze, gbc_lblFreieParkpltze);

		JLabel lblTragenSieBitte = new JLabel("Tragen sie bitte die neuen Daten ein");
		GridBagConstraints gbc_lblTragenSieBitte = new GridBagConstraints();
		gbc_lblTragenSieBitte.insets = new Insets(0, 0, 5, 0);
		gbc_lblTragenSieBitte.gridx = 5;
		gbc_lblTragenSieBitte.gridy = 1;
		contentPane.add(lblTragenSieBitte, gbc_lblTragenSieBitte);

		JLabel lblanzahl = new JLabel("[Anzahl]");
		GridBagConstraints gbc_lblanzahl = new GridBagConstraints();
		gbc_lblanzahl.insets = new Insets(0, 0, 5, 5);
		gbc_lblanzahl.gridx = 0;
		gbc_lblanzahl.gridy = 2;
		contentPane.add(lblanzahl, gbc_lblanzahl);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			String query = "SELECT COUNT(istBesetzt) FROM Parkplatz WHERE istBesetzt < '1'";
			PreparedStatement PS = CC.prepareStatement(query);
			ResultSet result = PS.executeQuery();
			result.next();
			lblanzahl.setText("" + result.getString(1));

			JButton btnZurck = new JButton("Zur\u00FCck");
			btnZurck.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PVerwalten p1 = new PVerwalten();
					p1.setVisible(true);
					dispose();
				}
			});
			GridBagConstraints gbc_btnZurck = new GridBagConstraints();
			gbc_btnZurck.insets = new Insets(0, 0, 5, 5);
			gbc_btnZurck.gridx = 0;
			gbc_btnZurck.gridy = 3;
			contentPane.add(btnZurck, gbc_btnZurck);

			JLabel lblBeschreibung = new JLabel("Beschreibung: ");
			GridBagConstraints gbc_lblBeschreibung = new GridBagConstraints();
			gbc_lblBeschreibung.anchor = GridBagConstraints.EAST;
			gbc_lblBeschreibung.insets = new Insets(0, 0, 5, 5);
			gbc_lblBeschreibung.gridx = 4;
			gbc_lblBeschreibung.gridy = 3;
			contentPane.add(lblBeschreibung, gbc_lblBeschreibung);

			JComboBox comboBox = new JComboBox();
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 0);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 5;
			gbc_comboBox.gridy = 7;
			contentPane.add(comboBox, gbc_comboBox);
			
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection CC1 = null;
				CC1 = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
				System.out.print("Database is connected!");

				Statement stmt = CC1.createStatement();
				String liste = "SELECT * FROM Parkplatz";
				ResultSet rs = stmt.executeQuery(liste);
				while (rs.next()) {
					comboBox.addItem(rs.getString("Bezeichnung"));

				}
				stmt.close();

				CC1.close();
			} catch (Exception e) {
				System.out.print("Connection to Database failed - Error:" + e);
			}
			
			
			
			
			

			editBeschreibung = new JTextField();
			GridBagConstraints gbc_editBeschreibung = new GridBagConstraints();
			gbc_editBeschreibung.insets = new Insets(0, 0, 5, 0);
			gbc_editBeschreibung.fill = GridBagConstraints.HORIZONTAL;
			gbc_editBeschreibung.gridx = 5;
			gbc_editBeschreibung.gridy = 3;
			contentPane.add(editBeschreibung, gbc_editBeschreibung);
			editBeschreibung.setColumns(10);

			JButton btnFortfahren = new JButton("Fortfahren");
			btnFortfahren.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					String a = editBeschreibung.getText();
					String x = comboBox.getSelectedItem().toString();

					try {
						Class.forName("com.mysql.cj.jdbc.Driver");
						Connection CC = null;
						CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
						System.out.print("Database is connected!");

						String update = ("UPDATE `Parkplatz` SET bezeichnung = '" + a + "' WHERE bezeichnung = '" + x
								+ "'");
						PreparedStatement PS = CC.prepareStatement(update);
						int result = PS.executeUpdate();

						CC.close();
					} catch (Exception e1) {
						System.out.print("Connection to Database failed - Error:" + e1);
					}

					Men� h1 = new Men�();
					h1.setVisible(true);
					Popup4 p4 = new Popup4();
					p4.setVisible(true);
					dispose();
				}

			});

			JLabel lblUmWelchenParkplatz = new JLabel("Um welchen Parkplatz handelt es sich?");
			GridBagConstraints gbc_lblUmWelchenParkplatz = new GridBagConstraints();
			gbc_lblUmWelchenParkplatz.insets = new Insets(55, 0, 5, 0);
			gbc_lblUmWelchenParkplatz.gridx = 5;
			gbc_lblUmWelchenParkplatz.gridy = 6;
			contentPane.add(lblUmWelchenParkplatz, gbc_lblUmWelchenParkplatz);

			
			

			GridBagConstraints gbc_btnFortfahren = new GridBagConstraints();
			gbc_btnFortfahren.gridx = 5;
			gbc_btnFortfahren.gridy = 8;
			contentPane.add(btnFortfahren, gbc_btnFortfahren);
			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		this.setTitle("Parkplatz Daten �ndern");
	}

}
