import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JComboBox;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class PAuswahl extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PAuswahl frame = new PAuswahl();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PAuswahl() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		

		JLabel lblParkplatzWhlen = new JLabel("Parkplatz w\u00E4hlen");
		GridBagConstraints gbc_lblParkplatzWhlen = new GridBagConstraints();
		gbc_lblParkplatzWhlen.insets = new Insets(0, 0, 5, 0);
		gbc_lblParkplatzWhlen.gridx = 1;
		gbc_lblParkplatzWhlen.gridy = 0;
		contentPane.add(lblParkplatzWhlen, gbc_lblParkplatzWhlen);

		JComboBox comboBox = new JComboBox();
		comboBox.setMaximumRowCount(30);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(10, 0, 55, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 2;
		contentPane.add(comboBox, gbc_comboBox);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			Statement stmt = CC.createStatement();
			String liste = "SELECT * FROM Parkplatz WHERE istBesetzt = '0'";
			ResultSet rs = stmt.executeQuery(liste);
			while (rs.next()) {
				comboBox.addItem(rs.getString("Bezeichnung"));

			}
			stmt.close();

			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JButton btnFortfahren = new JButton("Fortfahren");
		btnFortfahren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String x = comboBox.getSelectedItem().toString();

				try

				{
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection CC = null;
					CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
					System.out.print("Database is connected!");

					String update = ("UPDATE `Parkplatz` SET istBesetzt = 1 WHERE bezeichnung = '" + x + "'");
					PreparedStatement PS = CC.prepareStatement(update);
					int result = PS.executeUpdate();

					CC.close();
				} catch (Exception e1) {
					System.out.print("Connection to Database failed - Error:" + e1);
				}

				
				Men� m = new Men�();
				m.setVisible(true);
				Beanspruchen b1 = new Beanspruchen();
				b1.setVisible(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnFortfahren = new GridBagConstraints();
		gbc_btnFortfahren.gridx = 1;
		gbc_btnFortfahren.gridy = 4;
		contentPane.add(btnFortfahren, gbc_btnFortfahren);
	}

}
