import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;

public class Men� extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Men� frame = new Men�();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Men�() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		this.setTitle("Hauptmen�");

		JLabel lblFreieParkpltze = new JLabel("Freie Parkpl\u00E4tze");
		GridBagConstraints gbc_lblFreieParkpltze = new GridBagConstraints();
		gbc_lblFreieParkpltze.insets = new Insets(0, 0, 5, 5);
		gbc_lblFreieParkpltze.gridx = 1;
		gbc_lblFreieParkpltze.gridy = 1;
		contentPane.add(lblFreieParkpltze, gbc_lblFreieParkpltze);

		JLabel lblanzahl = new JLabel("[Anzahl]");
		GridBagConstraints gbc_lblanzahl = new GridBagConstraints();
		gbc_lblanzahl.insets = new Insets(0, 0, 5, 5);
		gbc_lblanzahl.gridx = 1;
		gbc_lblanzahl.gridy = 2;
		contentPane.add(lblanzahl, gbc_lblanzahl);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			String query = "SELECT COUNT(istBesetzt) FROM Parkplatz WHERE istBesetzt < '1'";
			PreparedStatement PS = CC.prepareStatement(query);
			ResultSet result = PS.executeQuery();
			result.next();
			lblanzahl.setText("" + result.getString(1));

			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JButton btnParkplatzBeanspruchen = new JButton("Parkplatz belegen");
		btnParkplatzBeanspruchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PAuswahl p1 = new PAuswahl();
				p1.setVisible(true);
				dispose();
			}
		});

		GridBagConstraints gbc_btnParkplatzBeanspruchen = new GridBagConstraints();
		gbc_btnParkplatzBeanspruchen.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnParkplatzBeanspruchen.insets = new Insets(0, 75, 5, 5);
		gbc_btnParkplatzBeanspruchen.gridx = 3;
		gbc_btnParkplatzBeanspruchen.gridy = 2;
		contentPane.add(btnParkplatzBeanspruchen, gbc_btnParkplatzBeanspruchen);

		JButton btnParkplatzFreigeben = new JButton("Parkplatz freigeben");
		btnParkplatzFreigeben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PAuswahl2 p2 = new PAuswahl2();
				p2.setVisible(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnParkplatzFreigeben = new GridBagConstraints();
		gbc_btnParkplatzFreigeben.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnParkplatzFreigeben.insets = new Insets(0, 75, 25, 5);
		gbc_btnParkplatzFreigeben.gridx = 3;
		gbc_btnParkplatzFreigeben.gridy = 3;
		contentPane.add(btnParkplatzFreigeben, gbc_btnParkplatzFreigeben);

		JButton btnAccountVerwalten = new JButton("Nutzer verwalten");
		btnAccountVerwalten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Verwalten v1 = new Verwalten();
				v1.setVisible(true);
				dispose();
			}
		});

		JButton btnNeuenNutzerAnlegen = new JButton("Nutzer anlegen");
		btnNeuenNutzerAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NeuerNutzer n1 = new NeuerNutzer();
				n1.setVisible(true);
				dispose();
			}
		});
		
		
		
		GridBagConstraints gbc_btnNeuenNutzerAnlegen = new GridBagConstraints();
		gbc_btnNeuenNutzerAnlegen.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNeuenNutzerAnlegen.insets = new Insets(0, 75, 5, 5);
		gbc_btnNeuenNutzerAnlegen.gridx = 3;
		gbc_btnNeuenNutzerAnlegen.gridy = 5;
		contentPane.add(btnNeuenNutzerAnlegen, gbc_btnNeuenNutzerAnlegen);

		GridBagConstraints gbc_btnAccountVerwalten = new GridBagConstraints();
		gbc_btnAccountVerwalten.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAccountVerwalten.insets = new Insets(0, 75, 5, 5);
		gbc_btnAccountVerwalten.gridx = 3;
		gbc_btnAccountVerwalten.gridy = 6;
		contentPane.add(btnAccountVerwalten, gbc_btnAccountVerwalten);

		JButton btnParkpltzeVerwalten = new JButton("Parkplatz verwalten");
		btnParkpltzeVerwalten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PVerwalten p3 = new PVerwalten();
				p3.setVisible(true);
				dispose();
			}
		});
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { btnParkplatzBeanspruchen,
				btnAccountVerwalten, btnParkplatzFreigeben, btnParkpltzeVerwalten }));
		GridBagConstraints gbc_btnParkpltzeVerwalten = new GridBagConstraints();
		gbc_btnParkpltzeVerwalten.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnParkpltzeVerwalten.insets = new Insets(0, 75, 5, 5);
		gbc_btnParkpltzeVerwalten.gridx = 3;
		gbc_btnParkpltzeVerwalten.gridy = 7;
		contentPane.add(btnParkpltzeVerwalten, gbc_btnParkpltzeVerwalten);
	}

}
