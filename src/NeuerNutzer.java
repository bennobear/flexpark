import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class NeuerNutzer extends JFrame {

	private JPanel contentPane;
	private JTextField neuName;
	private JTextField neuVorname;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NeuerNutzer frame = new NeuerNutzer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NeuerNutzer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		this.setTitle("Nutzer anlegen");

		JButton btnZurck = new JButton("Zur\u00FCck");
		btnZurck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Men� h1 = new Men�();
				h1.setVisible(true);
				dispose();
			}
		});

		JLabel lblFreieParkpltze = new JLabel("Freie Parkpl\u00E4tze");
		GridBagConstraints gbc_lblFreieParkpltze = new GridBagConstraints();
		gbc_lblFreieParkpltze.insets = new Insets(0, 0, 5, 5);
		gbc_lblFreieParkpltze.gridx = 1;
		gbc_lblFreieParkpltze.gridy = 1;
		contentPane.add(lblFreieParkpltze, gbc_lblFreieParkpltze);

		JLabel lblGebenSieBitte = new JLabel("Tragen sie bitte die Daten ein");
		GridBagConstraints gbc_lblGebenSieBitte = new GridBagConstraints();
		gbc_lblGebenSieBitte.insets = new Insets(0, 0, 5, 5);
		gbc_lblGebenSieBitte.gridx = 5;
		gbc_lblGebenSieBitte.gridy = 1;
		contentPane.add(lblGebenSieBitte, gbc_lblGebenSieBitte);

		JLabel lblanzahl = new JLabel("[Anzahl]");
		GridBagConstraints gbc_lblanzahl = new GridBagConstraints();
		gbc_lblanzahl.insets = new Insets(0, 0, 5, 5);
		gbc_lblanzahl.gridx = 1;
		gbc_lblanzahl.gridy = 2;
		contentPane.add(lblanzahl, gbc_lblanzahl);
		GridBagConstraints gbc_btnZurck = new GridBagConstraints();
		gbc_btnZurck.insets = new Insets(0, 0, 5, 5);
		gbc_btnZurck.gridx = 1;
		gbc_btnZurck.gridy = 4;
		contentPane.add(btnZurck, gbc_btnZurck);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			String query = "SELECT COUNT(istBesetzt) FROM Parkplatz WHERE istBesetzt < '1'";
			PreparedStatement PS = CC.prepareStatement(query);
			ResultSet result = PS.executeQuery();
			result.next();
			lblanzahl.setText("" + result.getString(1));

			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JLabel lblName = new JLabel("Name");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 50, 5, 5);
		gbc_lblName.gridx = 4;
		gbc_lblName.gridy = 4;
		contentPane.add(lblName, gbc_lblName);

		neuName = new JTextField();
		GridBagConstraints gbc_neuName = new GridBagConstraints();
		gbc_neuName.insets = new Insets(0, 0, 5, 5);
		gbc_neuName.fill = GridBagConstraints.HORIZONTAL;
		gbc_neuName.gridx = 5;
		gbc_neuName.gridy = 4;
		contentPane.add(neuName, gbc_neuName);
		neuName.setColumns(10);

		JLabel lblVorname = new JLabel("Vorname");
		GridBagConstraints gbc_lblVorname = new GridBagConstraints();
		gbc_lblVorname.insets = new Insets(0, 50, 5, 5);
		gbc_lblVorname.gridx = 4;
		gbc_lblVorname.gridy = 5;
		contentPane.add(lblVorname, gbc_lblVorname);

		neuVorname = new JTextField();
		GridBagConstraints gbc_neuVorname = new GridBagConstraints();
		gbc_neuVorname.insets = new Insets(0, 0, 5, 5);
		gbc_neuVorname.fill = GridBagConstraints.HORIZONTAL;
		gbc_neuVorname.gridx = 5;
		gbc_neuVorname.gridy = 5;
		contentPane.add(neuVorname, gbc_neuVorname);
		neuVorname.setColumns(10);

		JButton btnFortfahren = new JButton("Fortfahren");
		btnFortfahren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String a = neuVorname.getText();
				String b = neuName.getText();

				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection CC = null;
					CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
					System.out.print("Database is connected!");

					String update = ("INSERT INTO `Mitarbeiter` (`name`, `vorname`) VALUES ('" + b + "', '" + a + "')");
					PreparedStatement PS = CC.prepareStatement(update);
					int result = PS.executeUpdate();

					CC.close();
				} catch (Exception e1) {
					System.out.print("Connection to Database failed - Error:" + e1);
				}

				Men� h1 = new Men�();
				h1.setVisible(true);
				Popup p = new Popup();
				p.setVisible(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnFortfahren = new GridBagConstraints();
		gbc_btnFortfahren.insets = new Insets(25, 0, 0, 5);
		gbc_btnFortfahren.gridx = 5;
		gbc_btnFortfahren.gridy = 7;
		contentPane.add(btnFortfahren, gbc_btnFortfahren);
	}

}
