import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Beanspruchen extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Beanspruchen frame = new Beanspruchen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Beanspruchen() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 200, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
				contentPane.setLayout(null);
		
				JLabel lblParkplatzErfolgreichBeansprucht = new JLabel(
						"<HTML><BODY>Parkplatz erfolgreich<BR> belegt<BODY><HTML>");
				lblParkplatzErfolgreichBeansprucht.setBounds(42, 30, 100, 55);
				contentPane.add(lblParkplatzErfolgreichBeansprucht);
		
				JButton btnFortfahren = new JButton("Fortfahren");
				btnFortfahren.setBounds(42, 113, 100, 23);
				btnFortfahren.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						dispose();
					}
				});
				contentPane.add(btnFortfahren);
	}

}
