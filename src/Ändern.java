import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class �ndern extends JFrame {

	private JPanel contentPane;
	private JTextField editName;
	private JTextField editVorname;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					�ndern frame = new �ndern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public �ndern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		this.setTitle("Daten Bearbeitung - MA");

		JLabel lblFreieParkpltze = new JLabel("Freie Parkpl\u00E4tze");
		GridBagConstraints gbc_lblFreieParkpltze = new GridBagConstraints();
		gbc_lblFreieParkpltze.insets = new Insets(0, 0, 5, 5);
		gbc_lblFreieParkpltze.gridx = 0;
		gbc_lblFreieParkpltze.gridy = 1;
		contentPane.add(lblFreieParkpltze, gbc_lblFreieParkpltze);

		JLabel lblTragenSieBitte = new JLabel("Tragen sie bitte die neuen Daten ein");
		GridBagConstraints gbc_lblTragenSieBitte = new GridBagConstraints();
		gbc_lblTragenSieBitte.insets = new Insets(0, 0, 5, 0);
		gbc_lblTragenSieBitte.gridx = 5;
		gbc_lblTragenSieBitte.gridy = 1;
		contentPane.add(lblTragenSieBitte, gbc_lblTragenSieBitte);

		JLabel lblanzahl = new JLabel("[Anzahl]");
		GridBagConstraints gbc_lblanzahl = new GridBagConstraints();
		gbc_lblanzahl.insets = new Insets(0, 0, 5, 5);
		gbc_lblanzahl.gridx = 0;
		gbc_lblanzahl.gridy = 2;
		contentPane.add(lblanzahl, gbc_lblanzahl);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			String query = "SELECT COUNT(istBesetzt) FROM Parkplatz WHERE istBesetzt < '1'";
			PreparedStatement PS = CC.prepareStatement(query);
			ResultSet result = PS.executeQuery();
			result.next();
			lblanzahl.setText("" + result.getString(1));
			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}
		
				JButton btnZurck = new JButton("Zur\u00FCck");
				btnZurck.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Verwalten v1 = new Verwalten();
						v1.setVisible(true);
						dispose();
					}
				});
				GridBagConstraints gbc_btnZurck = new GridBagConstraints();
				gbc_btnZurck.insets = new Insets(0, 0, 5, 5);
				gbc_btnZurck.gridx = 0;
				gbc_btnZurck.gridy = 3;
				contentPane.add(btnZurck, gbc_btnZurck);

		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 5;
		gbc_comboBox.gridy = 7;
		contentPane.add(comboBox, gbc_comboBox);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			Statement stmt = CC.createStatement();
			String liste = "SELECT name, vorname FROM Mitarbeiter";
			ResultSet rs = stmt.executeQuery(liste);
			while (rs.next()) {
				comboBox.addItem(rs.getString("vorname") + ", " + rs.getString("name"));

			}
			stmt.close();

			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JLabel lblName = new JLabel("Name");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 4;
		gbc_lblName.gridy = 3;
		contentPane.add(lblName, gbc_lblName);

		editName = new JTextField();
		GridBagConstraints gbc_editName = new GridBagConstraints();
		gbc_editName.insets = new Insets(0, 0, 5, 0);
		gbc_editName.fill = GridBagConstraints.HORIZONTAL;
		gbc_editName.gridx = 5;
		gbc_editName.gridy = 3;
		contentPane.add(editName, gbc_editName);
		editName.setColumns(10);

		JLabel lblVorname = new JLabel("Vorname");
		GridBagConstraints gbc_lblVorname = new GridBagConstraints();
		gbc_lblVorname.anchor = GridBagConstraints.EAST;
		gbc_lblVorname.insets = new Insets(0, 0, 5, 5);
		gbc_lblVorname.gridx = 4;
		gbc_lblVorname.gridy = 4;
		contentPane.add(lblVorname, gbc_lblVorname);

		editVorname = new JTextField();
		GridBagConstraints gbc_editVorname = new GridBagConstraints();
		gbc_editVorname.insets = new Insets(0, 0, 5, 0);
		gbc_editVorname.fill = GridBagConstraints.HORIZONTAL;
		gbc_editVorname.gridx = 5;
		gbc_editVorname.gridy = 4;
		contentPane.add(editVorname, gbc_editVorname);
		editVorname.setColumns(10);

		JButton btnFortfahren = new JButton("Fortfahren");
		btnFortfahren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String a = editVorname.getText();
				String b = editName.getText();
				int y = comboBox.getSelectedIndex() + 1;
				System.out.println(y);

				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection CC = null;
					CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
					System.out.print("Database is connected!");

					String update = ("UPDATE `Mitarbeiter` SET name = '" + b + "', vorname = '" + a + "' WHERE '" + y
							+ "' = idMitarbeiter");
					PreparedStatement PS = CC.prepareStatement(update);
					int result = PS.executeUpdate();

					CC.close();
				} catch (Exception e1) {
					System.out.print("Connection to Database failed - Error:" + e1);
				}

				Men� h1 = new Men�();
				h1.setVisible(true);
				Popup3 p3 = new Popup3();
				p3.setVisible(true);
				dispose();
			}
		});

		JLabel lblUmWenHandelt = new JLabel("Um wen handelt es sich?");
		GridBagConstraints gbc_lblUmWenHandelt = new GridBagConstraints();
		gbc_lblUmWenHandelt.insets = new Insets(55, 0, 5, 0);
		gbc_lblUmWenHandelt.gridx = 5;
		gbc_lblUmWenHandelt.gridy = 6;
		contentPane.add(lblUmWenHandelt, gbc_lblUmWenHandelt);

		GridBagConstraints gbc_btnFortfahren = new GridBagConstraints();
		gbc_btnFortfahren.gridx = 5;
		gbc_btnFortfahren.gridy = 8;
		contentPane.add(btnFortfahren, gbc_btnFortfahren);
	}

}
