import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class Verwalten extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Verwalten frame = new Verwalten();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Verwalten() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);
		this.setTitle("Nutzer Verwaltung");

		JLabel lblFreieParkpltze = new JLabel("Freie Parkpl\u00E4tze");
		GridBagConstraints gbc_lblFreieParkpltze = new GridBagConstraints();
		gbc_lblFreieParkpltze.insets = new Insets(0, 0, 5, 5);
		gbc_lblFreieParkpltze.gridx = 1;
		gbc_lblFreieParkpltze.gridy = 1;
		contentPane.add(lblFreieParkpltze, gbc_lblFreieParkpltze);

		JLabel lblanzahl = new JLabel("[Anzahl]");
		GridBagConstraints gbc_lblanzahl = new GridBagConstraints();
		gbc_lblanzahl.insets = new Insets(0, 0, 5, 5);
		gbc_lblanzahl.gridx = 1;
		gbc_lblanzahl.gridy = 2;
		contentPane.add(lblanzahl, gbc_lblanzahl);

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection CC = null;
			CC = DriverManager.getConnection("jdbc:mysql://localhost/flexpark", "root", "");
			System.out.print("Database is connected!");

			String query = "SELECT COUNT(istBesetzt) FROM Parkplatz WHERE istBesetzt < '1'";
			PreparedStatement PS = CC.prepareStatement(query);
			ResultSet result = PS.executeQuery();
			result.next();

			lblanzahl.setText("" + result.getString(1));
			CC.close();
		} catch (Exception e) {
			System.out.print("Connection to Database failed - Error:" + e);
		}

		JButton btnZurck = new JButton("Zur\u00FCck");
		btnZurck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Men� m1 = new Men�();
				m1.setVisible(true);
				dispose();
			}
		});
		
				JButton btnDatenndern = new JButton("Nutzer bearbeiten");
				btnDatenndern.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						�ndern �1 = new �ndern();
						�1.setVisible(true);
						dispose();
					}
				});
				
						GridBagConstraints gbc_btnDatenndern = new GridBagConstraints();
						gbc_btnDatenndern.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnDatenndern.insets = new Insets(0, 75, 5, 5);
						gbc_btnDatenndern.gridx = 3;
						gbc_btnDatenndern.gridy = 2;
						contentPane.add(btnDatenndern, gbc_btnDatenndern);
		GridBagConstraints gbc_btnZurck = new GridBagConstraints();
		gbc_btnZurck.insets = new Insets(0, 0, 5, 5);
		gbc_btnZurck.gridx = 1;
		gbc_btnZurck.gridy = 3;
		contentPane.add(btnZurck, gbc_btnZurck);
		
				JButton btnAccountLschen = new JButton("Nutzer l\u00F6schen");
				btnAccountLschen.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						L�schen l1 = new L�schen();
						l1.setVisible(true);
						dispose();

					}
				});
				
						GridBagConstraints gbc_btnAccountLschen = new GridBagConstraints();
						gbc_btnAccountLschen.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnAccountLschen.insets = new Insets(20, 75, 5, 5);
						gbc_btnAccountLschen.gridx = 3;
						gbc_btnAccountLschen.gridy = 6;
						contentPane.add(btnAccountLschen, gbc_btnAccountLschen);
	}

}
